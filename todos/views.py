from django.shortcuts import (
    render,
    redirect,
    get_list_or_404,
    get_object_or_404,
)
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todos/item_update.html", context)
